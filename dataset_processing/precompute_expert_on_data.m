clc;
clear;
close all;

%% Load data
dataset = strcat(getenv('dataset_folder'), '/dataset1/');
grid_name = 'exploration_grid';
laser_name = 1;

load(strcat(dataset, 'data/train_data.mat'));
grid_params = get_grid_params(grid_name);
laser_params = get_laser_params(laser_name);
ig_params = get_laser_params(laser_name);
ig_params.laser_resolution = 10*laser_params.laser_resolution;

%% Lets set filename
global_option_set = 2; %Problem type
switch global_option_set
    case 1
        decision_budget = 30;
        state_transition = @(action, state, vertices, world_map) update_robot_belief( action, state, vertices, world_map, laser_params, grid_params );
        objective_fn = @(state, world_map) occupied_cell_count(state, world_map);
        cost_fn = @(state, vertices, world_map) 1;
        cost_budget = Inf;
        
        policy_actual_expert =  @(state0, action_ids, vertices, world_map) lazy_greedy_nemhauser( action_ids, @(state) objective_fn(state, world_map), ...
            state0, @(action, state) state_transition(action, state, vertices, world_map), decision_budget, ...
            @(state) cost_fn(state, vertices, world_map), cost_budget );
        precomputed_action_trajectory = [];
        
        filename = strcat(dataset,'precomputed_expert/greedy_train.mat');
    case 2
        decision_budget = 30;
        state_transition = @(action, state, vertices, world_map) update_robot_belief( action, state, vertices, world_map, laser_params, grid_params );
        objective_fn = @(state, world_map) occupied_cell_count(state, world_map);
        cost_fn = @(state, vertices, world_map) motion_cost( state, vertices );
        cost_budget = 2500;
        
        policy_actual_expert = @(state0, action_ids, vertices, world_map) lazy_generalized_cost_benefit( action_ids, @(state) objective_fn(state, world_map), @(state) tour_cost( state, vertices ), ...
            state0, @(action, state) state_transition(action, state, vertices, world_map), cost_budget, decision_budget, ...
            @(state) tour_cost( state, vertices ) );
        precomputed_action_trajectory = cell(1,length(environment_set)) ;
        
        filename = strcat(dataset,'precomputed_expert/lazy_gcb_train.mat');
    otherwise
        error('Bad Option');
end



%% Compute expert
parfor trial = 1:length(environment_set)
    disp(trial)
    world_map = environment_set(trial).world_map;
    vertices = environment_set(trial).vertices;
    state0 = environment_set(trial).state;
    original_action_id = 1:length(vertices);
    
    precomputed_action_trajectory{trial} = policy_actual_expert(state0, setdiff(original_action_id, state0.action_set), vertices, world_map);
end

save(filename, 'precomputed_action_trajectory');