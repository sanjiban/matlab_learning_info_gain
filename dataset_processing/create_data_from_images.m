clc
clear
close all

%% Lets set filename
dataset = strcat(getenv('dataset_folder'), '/dataset3/');
type = 'test' ;%train, test



grid_name = 'exploration_grid';
laser_name = 1;
switch (type)
    case 'train'
        rng(1);
    case 'test'
        rng(100);
end
num_vertices = 300;

switch (type)
    case 'train'
        num_maps = 10;
    case 'test'
        num_maps = 1;
end

num_episodes = 10;

grid_params = get_grid_params(grid_name);
laser_params = get_laser_params(laser_name);
original_action_id = 1:num_vertices;
num_trials = num_maps*num_episodes;
state_transition = @(action, state, vertices, world_map) update_robot_belief( action, state, vertices, world_map, laser_params, grid_params );

%% Prepare initial conditions
trial = 1;
environment_set = [];
for map_idx = 1:num_maps
    switch (type)
        case 'train'
            map_name = strcat(dataset_folder,'/images/map',num2str(map_idx),'.png');
        case 'test'
            map_name = strcat(dataset_folder,'/images/map_test.png');
    end
    
    [ world_map, robo_map ] = get_map_obj( map_name, grid_params );
    state0.robot_belief = robo_map;
    state0.action_set = [];
    
    for epsiode = 1:num_episodes
        vertices = [1 1 0; sample_random_valid_vertices(num_vertices-1, world_map)];
        state = state_transition(1, state0, vertices, world_map);
        
        environment_set(trial).world_map = world_map;
        environment_set(trial).vertices = vertices;
        environment_set(trial).state = state;
        
        trial = trial + 1;
    end
end

switch (type)
    case 'train'
        save(strcat(dataset_folder,'data/train_data.mat'), 'environment_set');
    case 'test'
        save(strcat(dataset_folder,'data/test_data.mat'), 'environment_set');
end


