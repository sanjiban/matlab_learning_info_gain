function [ action_set, state_trajectory, objective_trajectory, cost_trajectory ] = lazy_generalized_cost_benefit( action_library, objective_fn, cost_fn, state0, state_transition, cost_budget, decision_budget, tour_cost_fn )

%Warning is highly suboptimal
if (~exist('tour_cost_fn','var'))
    error('need tour cost fn. IDEA OVERLOAD COST FN')
end
if (~exist('decision_budget','var'))
    decision_budget = Inf;
end
state_trajectory = [];
objective_trajectory = [];
cost_trajectory = [];

state = state0;
val = objective_fn(state);
curr_cost = cost_fn(state);

recompute = 1;

marginal_gain_set = [action_library' inf*ones(length(action_library), 1) inf*ones(length(action_library), 1) zeros(length(action_library), 1)];
best_idx = 1;
best_till_now = -1;

while 1
    if (recompute == 1)
        recompute = 0;
        for i = 1:size(marginal_gain_set,1)
            if (best_till_now < marginal_gain_set(i,2))
                state_new = state_transition( marginal_gain_set(i,1), state);
                marginal_gain = objective_fn(state_new) - val;
                marginal_gain_set(i,3) = marginal_gain;
                cost_gain = cost_fn(state_new) - curr_cost;
                marginal_gain_set(i,4) = cost_gain;
                alpha = 0;
                ratio = (1-alpha)*(marginal_gain / cost_gain) + alpha*marginal_gain;
                marginal_gain_set(i,2) = ratio;
                
                if (ratio > best_till_now)
                    best_till_now = ratio;
                    best_idx = i;
                end
            else
                break;
            end
        end
    end
    
    best_action = marginal_gain_set(best_idx, 1);

    if (marginal_gain_set(best_idx, 4) + curr_cost < cost_budget && marginal_gain_set(best_idx, 3) > 1)
        state = state_transition(best_action, state);
        val = objective_fn(state);
        curr_cost = cost_fn(state);
        
        state_trajectory = [state_trajectory; state];
        objective_trajectory = [objective_trajectory; val];
        cost_trajectory = [cost_trajectory; curr_cost];
            
        %fprintf('Time step %d Value %f Cost %f\n', length(objective_trajectory), val, curr_cost);

        if (length(objective_trajectory) >= decision_budget)
            break;
        end
        recompute = 1;
    end
    
    marginal_gain_set(best_idx,:) = [];
    marginal_gain_set = sortrows(marginal_gain_set, -2);
    best_idx = 1;
    best_till_now = -1;
    
    action_library = setdiff(action_library, best_action);
    if (isempty(action_library))
        break;
    end
end


tmp.action_set = state.action_set( (length(state0.action_set)) : end); %Extract new actions + root
[~, tmp] = tour_cost_fn( tmp );
action_set = tmp.action_set; %rerouted
action_set(1) = []; %have already visited this action
        
state = state0;
objective_trajectory = [];
state_trajectory = [];
cost_trajectory = [];
for a = action_set
    state = state_transition(a, state);
    val = objective_fn(state);
    cost = cost_fn(state);
    state_trajectory = [state_trajectory; state];
    objective_trajectory = [objective_trajectory; val];
    cost_trajectory = [cost_trajectory; cost];
end

% conciously returning new actions in action_set

if (~isinf(decision_budget) && length(objective_trajectory) < decision_budget)
    % keep looping and populate stuff
    tau = length(objective_trajectory);
    state_trajectory = [state_trajectory; repmat(state, decision_budget - tau, 1)];
    objective_trajectory = [objective_trajectory; repmat(val, decision_budget - tau, 1)];
    cost_trajectory = [cost_trajectory; repmat(curr_cost, decision_budget - tau, 1)];
end

end


