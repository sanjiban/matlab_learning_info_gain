function [action_set, state_trajectory, objective_trajectory] = heuristic_with_motion_pen( action_library, objective_fn, state0, state_transition, budget,cost_fn, cost_budget  )
%GREEDY_NEMHAUSER Summary of this function goes here
%   Detailed explanation goes here
%   TODO: Can assume that same action is not desirable and can remove



state_trajectory = [];
objective_trajectory = [];

state = state0;
val = objective_fn(state);
curr_cost = cost_fn(state);

for k = 1:budget
    marginal_gain_set = [];
    cost_gain_set = [];
    for i = action_library
        state_new = state_transition(i, state);
        marginal_gain = objective_fn(state_new) - val;
        marginal_gain_set = [marginal_gain_set; marginal_gain];
        cost_gain = cost_fn(state_new) - curr_cost;
        cost_gain_set = [cost_gain_set; cost_gain];
    end
    
    marginal_gain_set = marginal_gain_set / sum(marginal_gain_set);
    cost_gain_set = cost_gain_set / sum(cost_gain_set);
    
    [~, best_idx] = max(marginal_gain_set - cost_gain_set);
    best_action = action_library(best_idx);
    
     
    state_new = state_transition(best_action, state);
    if (cost_fn(state_new) < cost_budget)
        state = state_new;
    end
    
    val = objective_fn(state);
    curr_cost = cost_fn(state);
    fprintf('Time step %d Value %f Cost %f\n', k, val, curr_cost);
   
    action_library = setdiff(action_library, best_action);
    
    state_trajectory = [state_trajectory; state];
    objective_trajectory = [objective_trajectory; val];
end

action_set = state.action_set;

end
