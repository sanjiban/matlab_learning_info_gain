function [action_set, state_trajectory, objective_trajectory] = random_greedy_buchbinder( num_vertices, objective_fn, state0, state_transition, budget )
%RANDOM_GREEDY Summary of this function goes here
%   Detailed explanation goes here

action_library = 1:num_vertices;

state_trajectory = [];
objective_trajectory = [];

state = state0;
val = objective_fn(state);

for k = 1:budget
    k
    marginal_gain_set = [];
    for i = action_library
        state_new = state_transition(i, state);
        marginal_gain = objective_fn(state_new) - val;
        marginal_gain_set = [marginal_gain_set; marginal_gain];
    end
    
    [sortedValues, sortIndex] = sort(marginal_gain_set(:),'descend');
    best_idx = sortIndex(randi(budget));
    best_action = action_library(best_idx);
    
    state = state_transition(best_action, state);
    val = objective_fn(state)
    
    action_library = setdiff(action_library, best_action);
    
    state_trajectory = [state_trajectory; state];
    objective_trajectory = [objective_trajectory; val];
end

action_set = state.action_set;

end

