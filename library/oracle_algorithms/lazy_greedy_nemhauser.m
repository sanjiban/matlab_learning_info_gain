function [action_set, state_trajectory, objective_trajectory] = lazy_greedy_nemhauser( action_library, objective_fn, state0, state_transition, budget, cost_fn, cost_budget )
%GREEDY_NEMHAUSER Summary of this function goes here
%   Detailed explanation goes here
%   TODO: Can assume that same action is not desirable and can remove

if (~exist('cost_budget','var'))
    cost_fn = @(state) 0;
    cost_budget = Inf;
end

state_trajectory = [];
objective_trajectory = [];

state = state0;
val = objective_fn(state);

marginal_gain_set = [action_library' inf*ones(length(action_library), 1)];
best_idx = 0;
best_till_now = -1;

for k = 1:budget
    for i = 1:size(marginal_gain_set,1)
        if (best_till_now < marginal_gain_set(i,2))
            state_new = state_transition( marginal_gain_set(i,1), state);
            marginal_gain = objective_fn(state_new) - val;
            marginal_gain_set(i,2) = marginal_gain;
            if (marginal_gain > best_till_now)
                best_till_now = marginal_gain;
                best_idx = i;
            end
        else
            break;
        end
    end

    best_action = marginal_gain_set(best_idx, 1);
 
    state_new = state_transition(best_action, state);
    if (cost_fn(state_new) < cost_budget)
        state = state_new;
    end
    
    val = objective_fn(state);
    cost = cost_fn(state);

    %fprintf('Time step %d Value %f Cost %f\n', k, val, cost);

    marginal_gain_set(best_idx, :) = [];
    marginal_gain_set = sortrows(marginal_gain_set, -2);
    best_idx = 0;
    best_till_now = -1;
    
    state_trajectory = [state_trajectory; state];
    objective_trajectory = [objective_trajectory; val];
end

action_set = state.action_set;

end

