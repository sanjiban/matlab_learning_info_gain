function [action_set, state_trajectory, objective_trajectory] = greedy_nemhauser( action_library, objective_fn, state0, state_transition, budget )
%GREEDY_NEMHAUSER Summary of this function goes here
%   Detailed explanation goes here
%   TODO: Can assume that same action is not desirable and can remove



state_trajectory = [];
objective_trajectory = [];

state = state0;
val = objective_fn(state);

for k = 1:budget
    marginal_gain_set = [];
    for i = action_library
        state_new = state_transition(i, state);
        marginal_gain = objective_fn(state_new) - val;
        marginal_gain_set = [marginal_gain_set; marginal_gain];
    end
    [~, best_idx] = max(marginal_gain_set);
    best_action = action_library(best_idx);
    
    state = state_transition(best_action, state);
    val = objective_fn(state);
    fprintf('Time step %d Value %f \n', k, val);

    action_library = setdiff(action_library, best_action);
    
    state_trajectory = [state_trajectory; state];
    objective_trajectory = [objective_trajectory; val];
end

action_set = state.action_set;

end

