function [ action_set, state_trajectory, objective_trajectory, cost_trajectory ] = generalized_cost_benefit( action_library, objective_fn, cost_fn, state0, state_transition, budget )


state_trajectory = [];
objective_trajectory = [];
cost_trajectory = [];

state = state0;
val = objective_fn(state);
curr_cost = cost_fn(state);

recompute = 1;

while 1

    if (recompute == 1)
        recompute = 0;
        marginal_gain_set = [];
        cost_gain_set = [];
        for i = action_library
            state_new = state_transition(i, state);

            marginal_gain = objective_fn(state_new) - val;
            marginal_gain_set = [marginal_gain_set marginal_gain];

            cost_gain = cost_fn(state_new) - curr_cost;
            cost_gain_set = [cost_gain_set cost_gain];
        end
    end
    
    [~, best_idx] = max(marginal_gain_set./cost_gain_set);
    best_action = action_library(best_idx);
    
    if (cost_gain_set(best_idx) + curr_cost < budget && marginal_gain_set(best_idx) > 1)
        best_action
        state = state_transition(best_action, state);
        %val = val + marginal_gain_set(best_idx);
        %curr_cost = curr_cost + cost_gain_set(best_idx);
        val = objective_fn(state);
        curr_cost = cost_fn(state_new);
        
        state_trajectory = [state_trajectory; state];
        objective_trajectory = [objective_trajectory; val];
        cost_trajectory = [cost_trajectory; curr_cost];
        
        recompute = 1;
    end
    
    action_library = setdiff(action_library, best_action);
    marginal_gain_set(best_idx) = [];
    cost_gain_set(best_idx) = [];
    length(action_library)
    if (isempty(action_library))
        break;
    end
end

action_set = state.action_set;

end


