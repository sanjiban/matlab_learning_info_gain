function [action_selected, ranked_actions] = greedy_policy_with_motion ( state, action_ids, vertices, criteria_fn, greedy_index )
%GREEDY_POLICY_WITH_MOTION Summary of this function goes here
%   Detailed explanation goes here

criteria = criteria_fn(state, action_ids, vertices);
criteria = criteria(:, greedy_index) + eps;
criteria = criteria / sum(criteria);

pos = [vertices(action_ids,1:2)]; 
dist = pos - repmat(vertices(state.action_set(end), 1:2), size(pos, 1), 1);
dist = sqrt(sum(dist.^2, 2));
dist = dist / sum(dist); %normalized

%take plain difference
criteria = criteria - dist;

% Do greedy on some criterion
[m] = max(criteria);
max_idx = find(criteria == m);
selected_idx = max_idx(randi(length(max_idx)));
action_selected = action_ids(selected_idx);

action_criteria_set = [action_ids' criteria];
action_criteria_set = sortrows(action_criteria_set, -2);
ranked_actions = action_criteria_set(:,1)';

end

