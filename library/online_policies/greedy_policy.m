function [action_selected, ranked_actions] = greedy_policy( state, action_ids, vertices, criteria_fn, greedy_index )
%HEURISTIC_POLICY Summary of this function goes here
%   Detailed explanation goes here

criteria = criteria_fn(state, action_ids, vertices);

% Do greedy on some criterion
[m] = max(criteria(:,greedy_index));
max_idx = find(criteria(:,greedy_index) == m);
selected_idx = max_idx(randi(length(max_idx)));
action_selected = action_ids(selected_idx);

action_criteria_set = [action_ids' criteria(:,greedy_index)];
action_criteria_set = sortrows(action_criteria_set, -2);
ranked_actions = action_criteria_set(:,1)';
end

