function plot_robot_belief( state, vertices )
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here

[~, p_] = logodds2prob(state.robot_belief.data);
imshow(p_'); hold on;
display_vertices(vertices(state.action_set(end), :));
plot(vertices(state.action_set,1), vertices(state.action_set,2), 'r');
end

