function plot_objective_trajectory_set( objective_trajectory_set, color )
%UNTITLED16 Summary of this function goes here
%   Detailed explanation goes here

switch color
    case 1
        c = [1 0.7 0.7];
    case 2
        c = [0.7  1 0.7];
    case 3
        c = [0.7 0.7 1];
    case 4
        c = [0.5 0.5 0.5];
    case 5
        c = [0.7  1 1];
end

hold on;
for i = 1:size(objective_trajectory_set,2)
    plot(objective_trajectory_set(:,i), 'Color', c);
end

end

