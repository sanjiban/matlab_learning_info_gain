function display_vertices( vertices, rad )
%DISPLAY_VERTICES Summary of this function goes here
%   Detailed explanation goes here

if (nargin < 2)
    rad = 5;
end

for i = 1:size(vertices, 1)
    v = vertices(i,:);
    scatter(v(1), v(2), 10*rad);
    plot([v(1); v(1) + rad*cos(v(3))], [v(2); v(2) + rad*sin(v(3))], 'r')  
end
end

