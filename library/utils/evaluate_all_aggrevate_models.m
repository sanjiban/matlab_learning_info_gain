clc
clear
close all

%% Default parameters that you can override
dataset =  '../../matlab_learning_info_gain_dataset/dataset3/';
grid_name = 'exploration_grid';
laser_name = 1;
rng(1);

%% Lets set filename
global_option_set = 4; %make this as large as possible but keep the remaining part of the code unchanged
switch global_option_set
    case 1
        option_problem_setup = 'coverage';
        decision_budget = 30;
        load(strcat(dataset, 'learnt_predictors/coverage/aggrevate_reg.mat'));
        option_policy = 'aggrevate_reg';
    case 2
        option_problem_setup = 'coverage';
        decision_budget = 30;
        load(strcat(dataset, 'learnt_predictors/coverage/aggrevate_cs_class.mat'));
        option_policy = 'aggrevate_cs_class';
    case 3
        option_problem_setup = 'motion_constrained_coverage';
        decision_budget = 30;
        cost_budget = 2500;
        model_name = strcat(dataset, 'learnt_predictors/coverage_with_motion/all_dump/aggrevate_reg_gcb/aggrevate_reg_');
        num_models = 112;
        option_policy = 'aggrevate_reg';
    case 4
        option_problem_setup = 'motion_constrained_coverage';
        decision_budget = 30;
        cost_budget = 2500;
        model_name = strcat(dataset, 'learnt_predictors/coverage_with_motion/all_dump/aggrevate_cs_class/aggr_cs_class_');
        num_models = 20;
        option_policy = 'aggrevate_cs_class';
    otherwise
        error('Bad Option');
end



%% Load all invariant parameters
load(strcat(dataset, 'data/test_data.mat'));
grid_params = get_grid_params(grid_name);
laser_params = get_laser_params(laser_name);
ig_params = get_laser_params(laser_name);
ig_params.laser_resolution = 10*laser_params.laser_resolution;

%% Create problem setup
switch option_problem_setup
    case 'coverage'
        state_transition = @(action, state, vertices, world_map) update_robot_belief( action, state, vertices, world_map, laser_params, grid_params );
        objective_fn = @(state, world_map) occupied_cell_count(state, world_map);
        cost_fn = @(state, vertices, world_map) 1;
        cost_budget = Inf;
    case 'motion_constrained_coverage'
        state_transition = @(action, state, vertices, world_map) update_robot_belief( action, state, vertices, world_map, laser_params, grid_params );
        objective_fn = @(state, world_map) occupied_cell_count(state, world_map);
        cost_fn = @(state, vertices, world_map) motion_cost( state, vertices );
    otherwise
        error('Bad Option');
end

%% Create learner
is_policy_offline = 0;
switch option_policy
    case 'aggrevate_reg'
        policy_learner = @(state, action_ids, vertices, model) predict_rf_qval( state, action_ids, vertices, model.feature_fn, model );
    case 'aggrevate_cs_class'
        policy_learner = @(state, action_ids, vertices, model) predict_rf_cost( state, action_ids, vertices, model.feature_fn, model );
    otherwise
        error('Bad Option');
end

%% Evaluate
final_obj_set = zeros(num_models, length(environment_set));
parfor i = 1:num_models
    model = load_model(strcat(model_name,num2str(i),'.mat'));
    policy_learner_current = @(state, action_ids, vertices)  policy_learner(state, action_ids, vertices, model);
    rollout_with_learner_fn = @(state0, action_ids, vertices, world_map, tau) rollout_with_learner( state0, action_ids, vertices, world_map, tau, policy_learner_current, state_transition, cost_fn, cost_budget );
    final_obj = zeros(1, length(environment_set));
    for env_idx = 1:length(environment_set)
        world_map = environment_set(env_idx).world_map;
        state0 = environment_set(env_idx).state;
        vertices = environment_set(env_idx).vertices;
        original_action_id = 1:length(vertices);
        action_id = setdiff(original_action_id, state0.action_set);
        
        state = rollout_with_learner_fn(state0, action_id, vertices, world_map, decision_budget);
        final_obj(env_idx) = objective_fn(state, world_map);
    end
    final_obj_set(i, :) = final_obj;
    fprintf('\n Model: %d Mean score: %f \n', i, mean(final_obj));
end






