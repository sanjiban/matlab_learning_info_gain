function new_belief = update_robot_belief( action_id, old_belief, vertices, world_map, laser_params, grid_params )
%UPDATE_ROBOT_BELIEF Summary of this function goes here
%   Detailed explanation goes here

raycast_vertex = vertices(action_id, :);
[ray.trace_x, ray.trace_y, ray.trace_r, ray.trace_collision] = ...
fast_trace(world_map, laser_params.laser_fov, laser_params.laser_resolution, laser_params.max_range, laser_params.range_resolution, ...
[raycast_vertex(1) raycast_vertex(2)], raycast_vertex(3));

new_belief = old_belief;

new_belief.action_set = [new_belief.action_set action_id];

%gridmap update
new_belief.robot_belief.data = update_robo_world(laser_params.laser_fov, laser_params.laser_resolution, laser_params.range_resolution, ...
                         ray.trace_x, ray.trace_y, ray.trace_r, ray.trace_collision, ...
                         [raycast_vertex(1) raycast_vertex(2)], raycast_vertex(3), old_belief.robot_belief.data, world_map, grid_params);

end

