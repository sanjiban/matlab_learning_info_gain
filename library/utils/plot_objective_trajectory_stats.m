function plot_objective_trajectory_stats( objective_trajectory_set, color )
%UNTITLED18 Summary of this function goes here
%   Detailed explanation goes here

switch color
    case 1
        c = [0 0 0];
    case 2
        c = [0 1 0];
    case 3
        c = [0 0 1];
    case 4
        c = [1 0 0];
    case 5
        c = [1 0 1];
    case 6
        c = [1 1 0];
    case 7
        c = [0 1 1];
    case 8
        c = [0.5 0.5 1];
end

hold on;
mu = mean(objective_trajectory_set,2);
st = std(objective_trajectory_set,0,2);
%st = zeros(size(st_orig));
%st(1:5:end) = st_orig(1:5:end);
errorbar(mu, st, 'Color', c);


end

