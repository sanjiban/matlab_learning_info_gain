function [value, state] = rro_reward_handle( nodes, state, obj_fn, state_transition_loc, state0 )
%UNTITLED19 Summary of this function goes here
%   Detailed explanation goes here

if (isempty(state))
    state = state0;
end

state_bk = state;
for i = 1:size(nodes,1)
    state = state_transition_loc(nodes(i,:), state);
end

v1 = obj_fn(state);
v2 = obj_fn(state_bk);
value = v1 - v2;

state.action_set
end

