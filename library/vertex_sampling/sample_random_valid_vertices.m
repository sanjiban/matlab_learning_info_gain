function vertices = sample_random_valid_vertices( num_vertices, world_map )
%SAMPLE_RANDOM_VALID_VERTICES Summary of this function goes here
%   Detailed explanation goes here

count = 0;
vertices = [];
while (count < num_vertices)
    vertex = [randi(size(world_map.data,1)), randi(size(world_map.data,2)), rand()*2*pi - pi];
    if (~collision_check_binary(world_map.data, vertex(1), vertex(2)))
        vertices = [vertices; vertex];
        count = count + 1;
    end
end

end

