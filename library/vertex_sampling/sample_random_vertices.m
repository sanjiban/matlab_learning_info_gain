function vertices = sample_random_vertices( num_vertices, world_map )
%SAMPLE_RANDOM_VERTICES Summary of this function goes here
%   Detailed explanation goes here

vertices = [randi(size(world_map.data,1), [num_vertices,1]), ...
            randi(size(world_map.data,2), [num_vertices,1]), ...
            rand(num_vertices,1)*2*pi - pi];

end

