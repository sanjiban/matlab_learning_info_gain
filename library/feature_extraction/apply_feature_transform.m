function feature_new = apply_feature_transform( feature, model )
%UNTITLED11 Summary of this function goes here
%   feature is a cell array

n = length(feature);
for i = 1:n
    f = feature{i}; 
    f_new = [ones(size(f,1),1) (f - repmat(model.mu, size(f,1), 1))*model.T];
    feature_new{i} = f_new;
end

end

