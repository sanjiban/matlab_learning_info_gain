function pos_features = get_vertex_pose_features( state, action_set, vertices)
%GET_VERTEX_POSE_FEATURES Summary of this function goes here
%   action_set is vector of N length
%   pos_features is matrix of Nxd where N is number of actions, d is
%   feature dim
%   assumption - state has to have an action

anchor = vertices(state.action_set(end),:); %assume x,y,psi
pos = vertices(action_set, :);
rel_pos = pos(:,1:2) - repmat(anchor(1:2),size(pos,1), 1);
lx = dot(rel_pos, repmat([cos(anchor(3)) sin(anchor(3))], size(rel_pos,1), 1), 2);
ly = dot(rel_pos, repmat([-sin(anchor(3)) cos(anchor(3))], size(rel_pos,1), 1), 2);
lnorm = sqrt(rel_pos(:,1).^2 + rel_pos(:,2).^2);
del_thet = atan2(ly, lx);
del_psi = wrapToPi(pos(:,3) - anchor(3));

pos_features = [lx ly lnorm del_thet del_psi];
pos_features = [pos_features pos_features.^2];
end

