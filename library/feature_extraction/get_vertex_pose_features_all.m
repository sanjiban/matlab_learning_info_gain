function pos_features = get_vertex_pose_features_all( state, action_set, vertices)
%GET_VERTEX_POSE_FEATURES Summary of this function goes here
%   action_set is vector of N length
%   pos_features is matrix of Nxd where N is number of actions, d is
%   feature dim
%   assumption - state has to have an action

pos = vertices(action_set, :);
pos_features = [repmat([reshape(vertices(state.action_set, :), 1, [])], size(pos,1), 1) pos]; 

end

