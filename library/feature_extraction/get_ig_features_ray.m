function ig_features = get_ig_features_ray(angle_range,angle_resolution,max_range,range_resolution,position,yaw,robo_map, grid_params)

ranges = [0:range_resolution:max_range]';
angles = angle_range(1):angle_resolution:angle_range(2);

[p, p_] = logodds2prob(robo_map.data);

ig_o = zeros(size(position,1), size(angles,2));
ig_v = zeros(size(position,1), size(angles,2));
ig_k = zeros(size(position,1), size(angles,2));
ig_b = zeros(size(position,1), size(angles,2));
ig_n = zeros(size(position,1), size(angles,2));



for j=1:size(position,1)
    angles2 = bsxfun(@plus, angles,yaw(j));
    for i=1:size(angles2,2)
        xy = bsxfun(@plus,position(j,1:2),bsxfun(@times,ranges,[cos(angles2(i)),sin(angles2(i))]));
        xy = coord2index(xy(:,1),xy(:,2),robo_map.scale,robo_map.min(1),robo_map.min(2));
        [outId1,~] = find(xy(:,1)<1|xy(:,2)<1,1,'first');
        [outId2,~] = find(xy(:,1)>=size(robo_map.data,1)|xy(:,2)>=size(robo_map.data,2),1,'first');
        xy(:,1) = min(size(robo_map.data, 1)*ones(size(xy(:,1))), max(ones(size(xy(:,1))), xy(:,1)));
        xy(:,2) = min(size(robo_map.data, 2)*ones(size(xy(:,2))), max(ones(size(xy(:,2))), xy(:,2)));
        
        ind = sub2ind(size(robo_map.data),xy(:,1),xy(:,2));
        [collisionIdOrig,~] = find(robo_map.data(ind)>0,1,'first');
        collisionId = min([collisionIdOrig;outId1;outId2]);
       
        if (collisionId == 1)
            continue; %nothing to compute
        end
        
        if ~isempty(collisionId)
            ind = ind(1:collisionId-1);
        end
        
        if ~isempty(collisionIdOrig)
            ray_didcollide = 1;
        else
            ray_didcollide = 0;
        end
        
        ray_logodds = robo_map.data(ind);
        ray_ig_o = -log(p(ind)) + p_(ind).*ray_logodds;
        ray_p_ = p_(ind);
        ray_pv = cumprod(ray_p_);
        ray_ig_v = ray_pv.*ray_ig_o;
        
        ray_ig_u = abs(ray_logodds - grid_params.log_odds_occ) < eps;
        ray_ig_k = ray_ig_u.*ray_ig_v;
        
        ray_ig_b = ray_ig_u*ray_didcollide;
        
        ray_ig_n = ray_ig_b.*ray_ig_v;
        
        ig_o(j,i) = ig_o(j,i) + sum(ray_ig_o);
        ig_v(j,i) = ig_v(j,i) + sum(ray_ig_v);
        ig_k(j,i) = ig_k(j,i) + sum(ray_ig_k);
        ig_b(j,i) = ig_b(j,i) + sum(ray_ig_b);
        ig_n(j,i) = ig_n(j,i) + sum(ray_ig_n);
    end
end

ig_features = [ig_o ig_v ig_k ig_b ig_n];

