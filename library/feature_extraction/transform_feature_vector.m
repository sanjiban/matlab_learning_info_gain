function [transformed_feature, T, mu] = transform_feature_vector( feature )
%UNTITLED10 Summary of this function goes here
%   Transform by feature_new = (feature - mu)*T

feature_matrix = [];
for i = 1:length(feature);
    feature_matrix = [feature_matrix; feature{i}];
end

[~, mu, ~, T] = whiten(feature_matrix);

for i = 1:length(feature);
    transformed_feature{i} = (feature{i} - repmat(mu, size(feature{i},1), 1))*T;
    transformed_feature{i} = [ones(size(feature{i},1), 1) transformed_feature{i}];
end

end

