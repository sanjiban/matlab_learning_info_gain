function compact_model_set = compactify_rf_modelset( model_set )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

for i = 1:length(model_set)
    compact_model_set{i} = model_set{i};
    compact_model_set{i}.rf = compact(compact_model_set{i}.rf);
end

end

