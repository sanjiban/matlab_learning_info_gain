function state = rollout_mixture( state0, action_ids, vertices, world_map, tau, rollout_with_expert_fn, rollout_with_learner_fn, beta )
%ROLLOUT_MIXTURE Summary of this function goes here
%   Detailed explanation goes here

if (rand() <= beta)
    state = rollout_with_expert_fn(state0, action_ids, vertices, world_map, tau);
else
    state = rollout_with_learner_fn(state0, action_ids, vertices, world_map, tau);
end

end

