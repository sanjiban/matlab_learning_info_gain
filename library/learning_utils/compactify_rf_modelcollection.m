function compact_model_collection = compactify_rf_modelcollection( model_collection )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

for i = 1:length(model_collection)
    compact_model_collection(i) = model_collection(i);
    compact_model_collection(i).rf = compact(compact_model_collection(i).rf);
end

end