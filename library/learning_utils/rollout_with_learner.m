function state = rollout_with_learner( state0, action_ids, vertices, world_map, tau, policy_learner, state_transition, cost_fn, cost_budget )
%ROLLOUT_WITH_LEARNER Summary of this function goes here
%   policy_learner(state, action_ids, vertices)

state = state0;
original_action_ids = action_ids;
for t = 1:tau
    action_ids = setdiff(original_action_ids, state.action_set);
    action_selected = policy_learner(state, action_ids, vertices);
    state_new = state_transition(action_selected, state, vertices, world_map);
    m_cost = cost_fn(state_new, vertices, world_map);
    if (m_cost <= cost_budget)
        state = state_new;
    else
        break;
    end
end


end

