function state = rollout_with_expert_precomputed( state0, action_ids, vertices, world_map, tau, action_trajectory, state_transition )
%ROLLOUT_WITH_EXPERT Summary of this function goes here
%   Detailed explanation goes here

state = state0;
if (tau == 0)
    return;
end
                              
% already guaranteed that action trajectory wont exceed cost_budget
for t = 1:tau
    state = state_transition(action_trajectory(t), state, vertices, world_map);
    if (length(action_trajectory) <= t)
        break;
    end
end

end

