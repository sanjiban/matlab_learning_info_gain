function data = data_aggregator( data, data_new, type )
%UNTITLED11 Summary of this function goes here
%   Detailed explanation goes here

switch (type)
    case 'cs_class'
        id = length(data.feature_table) + 1;
        for i = 1:length(data_new)
            data.feature_table{id} = data_new{i}.feature_table{1};
            data.cost_table{id} = data_new{i}.cost_table{1};
            id = id + 1;
        end
    case 'regression'
        id = length(data.feature_set) + 1;
        for i = 1:length(data_new)
            data.feature_set{id} = data_new{i}.feature_set{1};
            data.qval{id} = data_new{i}.qval{1};
            id = id + 1;
        end
end

end

