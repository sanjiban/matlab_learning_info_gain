function [model, selected_action, train_risk, obj] = train_cs_classification( data, surrogate_loss, lambda )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

model.name = 'linear_cs_class';
[feature_table, model.T, model.mu] = transform_feature_vector( data.feature_table );
[model.weights,obj,~] = train_linear_primal_sg(feature_table, data.cost_table, lambda, surrogate_loss);
%(num_trials+pred); plot(obj); title(pred); pause(0.2);
figure(200); plot(obj);
[selected_idx,scores] = linear_scorer_predict(feature_table, model.weights);
train_risk = calc_true_risk(data.cost_table, selected_idx);
selected_action = data.action_ids(sub2ind(size(data.action_ids), 1:length(selected_idx), selected_idx));
end

