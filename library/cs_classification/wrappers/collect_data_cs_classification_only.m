function data = collect_data_cs_classification_only( data, state, action_ids, world_map, vertices, feature_fn, qval_fn, tau)
%UNTITLED3 Summary of this function goes here
%   data - cs classifciation data struct. this function will append to it
%   and in a fashion that the data user can parse

id = length(data.feature_table) + 1;
data.feature_table{id} = feature_fn(state, action_ids, vertices);
qval = qval_fn(state, action_ids, world_map, vertices, tau);
cost = max(qval) - qval;
data.cost_table{id} = cost;

end
