function model = train_rf_for_csclass_only( data, ntrees )
%TRAIN_RF_FOR_CSCLASS Summary of this function goes here
%   Detailed explanation goes here

model.name = 'rf_reg_cs_class';
%[feature_table, model.T, model.mu] = transform_feature_vector( data.feature_table );
feature_table = data.feature_table;
feat_set = [];
cost_set = [];
for idx = 1:length(feature_table)
    feat_set = [feat_set; feature_table{idx}];
    v = data.cost_table{idx};
    cost_set = [cost_set; v'];
end
model.rf = TreeBagger(ntrees, feat_set, cost_set, 'Method', 'regression');
end

