function [model, selected_action, train_risk] = train_rf_for_regress_only( data, ntrees )
%TRAIN_RF_FOR_CSCLASS Summary of this function goes here
%   Detailed explanation goes here

model.name = 'rf_reg';
feat_set = [];
qval_set = [];
for idx = 1:length(data.feature_set)
    feat_set = [feat_set; data.feature_set{idx}];
    qval_set = [qval_set; data.qval{idx}];
end
model.rf = TreeBagger(ntrees, feat_set, qval_set, 'Method', 'regression');

%Get train risk
predicted_train = predict(model.rf, feat_set);
train_risk = norm(qval_set - predicted_train);

end

