function data = collect_data_regression_only( data, state, action_ids, world_map, vertices, feature_fn, qval_fn, tau)
%UNTITLED3 Summary of this function goes here
%   data - cs classifciation data struct. this function will append to it
%   and in a fashion that the data user can parse

id = length(data.feature_set) + 1;

action = action_ids(randi(length(action_ids)));

data.feature_set{id} = feature_fn(state, action, vertices);

qval = qval_fn(state, action, world_map, vertices, tau);
data.qval{id} = qval;
end

