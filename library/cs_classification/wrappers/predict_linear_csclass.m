function [action_selected, ranked_actions] = predict_linear_csclass( state, action_ids, vertices, feature_fn, model )
%PREDICT_LINEAR_CSCLASS Summary of this function goes here
%   Detailed explanation goes here

feature_table = feature_fn(state, action_ids, vertices);
feature_table{1} = features;
feature_table = apply_feature_transform( feature_table, model );
[selected_idx, scores] = linear_scorer_predict(feature_table, model.weights);
selected_action = action_ids(selected_idx);

action_criteria_set = [action_ids' scores'];
action_criteria_set = sortrows(action_criteria_set, -2);
ranked_actions = action_criteria_set(:,1)';
end

