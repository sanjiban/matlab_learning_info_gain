function [cost, state] = partial_tour_cost( state, vertices, original_state )
%PARTIAL_TOUR_COST Summary of this function goes here
%   Detailed explanation goes here
% ASSUMPTION: state = original_state + stuff;
additional_state = state;
additional_state.action_set(1:(length(original_state.action_set)-1)) = [];
[val_tour, state_tour] = tour_cost(additional_state, vertices);
cost = motion_cost( original_state, vertices ) + val_tour;
state.action_set = [original_state.action_set(1:(end-1)) state_tour.action_set];

end

