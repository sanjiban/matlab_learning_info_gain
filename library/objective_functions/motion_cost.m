function val = motion_cost( state, vertex_set )
%MOTION_COST Summary of this function goes here
%   Detailed explanation goes here

if (length(state.action_set)<=1)
    val = 0;
else
    pos = [vertex_set(state.action_set,1:2)]; 
    d = diff(pos);
    val = sum(sqrt(d(:,1).^2 + d(:,2).^2));
end

end

