function [ qval ] = qval_greedy_objectivefn( state, action_ids, world_map, vertices, objective_fn, state_transition )
%QVAL_GREEDY_OBJECTIVEFN Summary of this function goes here
%   Detailed explanation goes here

qval = [];
val = objective_fn(state, world_map);
for a = action_ids
    state_new = state_transition(a, state, vertices, world_map);
    qval = [qval (objective_fn(state_new, world_map) - val)];
end

end

