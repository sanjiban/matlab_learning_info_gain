function val = occupied_cell_count( state, world_map )
%OCCUPIED_CELL_COUNT Summary of this function goes here
%   Detailed explanation goes here

threshold_map = state.robot_belief.data;
threshold_map(threshold_map <= 2) = 0;
threshold_map(threshold_map > 2) = 1;

val = nnz(threshold_map);

end

