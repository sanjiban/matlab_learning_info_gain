function [val, state] = tour_cost( state, vertex_set )
%TOUR_COST Summary of this function goes here
%   Detailed explanation goes here

if (length(state.action_set) <= 1)
    val = 0;
else
    pos = vertex_set(state.action_set,1:2);
    if (length(state.action_set) == 2)
        val = norm(pos(2,:) - pos(1,:));
    elseif (length(state.action_set) == 3)
        val = min(norm(pos(2,:) - pos(1,:)),norm(pos(3,:) - pos(1,:))) ...
                   + norm(pos(3,:) - pos(2,:));
        if (norm(pos(2,:) - pos(1,:)) > norm(pos(3,:) - pos(1,:)))
            state.action_set(2:3) = [state.action_set(3) state.action_set(2)];
        end
    else
        pos = flipud(pos);
        [~,ind,val] = solveTSP( pos, false ); 
        ind = length(ind)+1 - ind;
        ind = flipud(ind);
        state.action_set = state.action_set(ind);
    end
end

end

