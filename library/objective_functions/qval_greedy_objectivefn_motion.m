function [ qval ] = qval_greedy_objectivefn_motion( state, action_ids, world_map, vertices, objective_fn, state_transition )
%QVAL_GREEDY_OBJECTIVEFN Summary of this function goes here
%   Detailed explanation goes here

qval = [];
val = objective_fn(state, world_map);

gain = [];
dist = [];
for a = action_ids
    state_new = state_transition(a, state, vertices, world_map);
    gain = [gain (objective_fn(state_new, world_map) - val)];
    dist = [dist norm(vertices(a, 1:2) - vertices(state.action_set(end), 1:2))];
end

all_possible_action_ids = setdiff(1:size(vertices,1), state.action_set);
sum_gain = 0;
sum_dist = 0;
for a = all_possible_action_ids
    state_new = state_transition(a, state, vertices, world_map);
    sum_gain = sum_gain + (objective_fn(state_new, world_map) - val);
    sum_dist = sum_dist + norm(vertices(a, 1:2) - vertices(state.action_set(end), 1:2));
end

qval = gain/sum_gain - dist/sum_dist;

end

