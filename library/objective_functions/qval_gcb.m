function [ qval ] = qval_gcb( state, action_ids, world_map, vertices, objective_fn, state_transition, cost_budget, tau )
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here

qval = [];
val = objective_fn(state, world_map);
original_action_ids = setdiff(1:size(vertices,1), state.action_set);

for a = action_ids
    state_new = state_transition(a, state, vertices, world_map);
    cost_fn = @(s) partial_tour_cost( s, vertices, state_new );
    pure_tour_cost_fn = @(s) tour_cost( s, vertices );
    rew = objective_fn(state_new, world_map) - val;
    action_library = setdiff(original_action_ids, a);
    [ action_set, ~, objective_trajectory, ~ ] = lazy_generalized_cost_benefit( action_library, @(state) objective_fn(state, world_map), ...
        cost_fn, state_new, @(action, state) state_transition(action, state, vertices, world_map), cost_budget, tau,  pure_tour_cost_fn);
    qval_a = 0;
    if(size(objective_trajectory) > 0)
        qval_a = objective_trajectory(end) - val;
    elseif(cost_fn(state_new) <= cost_budget)
        qval_a = rew;
    end
    qval = [qval qval_a];
    fprintf('Qval: %f R: %d \n', qval_a, rew);
end

end

