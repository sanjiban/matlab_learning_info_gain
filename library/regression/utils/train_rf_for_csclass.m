function [model, selected_action, train_risk] = train_rf_for_csclass( data, ntrees )
%TRAIN_RF_FOR_CSCLASS Summary of this function goes here
%   Detailed explanation goes here

model.name = 'rf_reg_cs_class';
%[feature_table, model.T, model.mu] = transform_feature_vector( data.feature_table );
feature_table = data.feature_table;
feat_set = [];
cost_set = [];
for idx = 1:length(feature_table)
    feat_set = [feat_set; feature_table{idx}];
    cost_set = [cost_set; data.cost_table(idx,:)'];
end
model.rf = TreeBagger(ntrees, feat_set, cost_set, 'Method', 'regression');

selected_idx = [];
for idx = 1:length(feature_table)
    predicted_cost = predict(model.rf, feature_table{idx});
    [~, min_idx] = min(predicted_cost);
    selected_idx = [selected_idx min_idx];
end

train_risk = calc_true_risk(data.cost_table, selected_idx);
selected_action = data.action_ids(sub2ind(size(data.action_ids), 1:length(selected_idx), selected_idx));

end

