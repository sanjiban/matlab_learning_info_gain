function [action_selected, ranked_actions] = predict_rf_cost( state, action_ids, vertices, feature_fn, model )
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here

if (isempty(model))
    ranked_actions = action_ids(randperm(length(action_ids)));
    action_selected = ranked_actions(1);
    return;
end

feature_table = feature_fn(state, action_ids, vertices);
cost = predict(model.rf, feature_table);
[~, min_idx] = min(cost);
action_selected = action_ids(min_idx);

action_criteria_set = [action_ids' cost];
action_criteria_set = sortrows(action_criteria_set, 2);
ranked_actions = action_criteria_set(:,1)';
end

