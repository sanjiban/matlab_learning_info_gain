%% Install matlab safe navigation
old_folder = cd('../matlab_safe_navigation');

%Add required paths
addpath(genpath(strcat(pwd,'/fastMarching')));
addpath(genpath(strcat(pwd,'/maps')));
addpath(genpath(strcat(pwd,'/normalPathLibrary')));
addpath(genpath(strcat(pwd,'/gridmapping')));
addpath(genpath(strcat(pwd,'/rayCasting')));
addpath(genpath(strcat(pwd,'/infoGain')));
addpath(genpath(strcat(pwd,'/demos/rhcp/utils')));
addpath(genpath(strcat(pwd,'/demos/informationgain/utils')));
addpath(genpath(strcat(pwd,'/demos/informationgain/disp_utils')));
addpath(genpath(strcat(pwd,'/frontierExploration')));

cd(old_folder);
%% Setup paths here
addpath(genpath(strcat(pwd,'/library')));

%% Setup datasets path
setenv('dataset_folder','/Volumes/NO NAME/matlab_learning_info_gain_dataset');
