# Learning to Gather Information via Imitation #

S. Choudhury, A. Kapoor, G. Ranade, D. Dey. "Learning to Gather Information via Imitation". In ICRA, 2017.

S. Choudhury, A. Kapoor, G. Ranade, S.Scherer, D. Dey. "Adaptive Information Gathering via Imitation Learning". In RSS, 2017.

For C++ codebase, check

[https://bitbucket.org/sanjiban/ig_learning_experiments](https://bitbucket.org/sanjiban/ig_learning_experiments)

## Installation ##

Create a folder to contain all repositories and cd into it.
```
#!bash

mkdir matlab_learning_info_gain_ws
cd matlab_learning_info_gain_ws
```
Clone this repository

```
#!bash

git clone https://bitbucket.org/sanjiban/matlab_learning_info_gain.git
```
Clone a dependency repository used for the backend of exploration simulation

```
#!bash

git clone https://bitbucket.org/castacks/matlab_safe_navigation.git
```
## Getting Datasets ##

Clone the datasets repository 

```
#!bash

git clone https://bitbucket.org/sanjiban/matlab_learning_info_gain_dataset.git
```

Once you have the datasets, add the path to the datasets folder to the environment variable dataset_folder in init_setup.m 

## Setup required on starting MATLAB ##
1. Start Matlab
2. Go to the folder matlab_learning_info_gain
3. Everytime you startup MATLAB, you have to run the script init_setup.m
This ensures that all the relative paths are set. 

## Running the Code ##
We now describe a quick start to running the code. There are 3 kinds of operations that you can do - visualize a policy on a datapoint, test a policy on a dataset or train a policy on the dataset. If you want to create a dataset from scratch, look at the next section. We recommend you start of by visualizing a policy.

### 1. Visualizing a policy on a datapoint ###
To visualize a policy on a datapoint, go to main/visualize_policy.m 
The script is self explanatory. The dataset already has pretrained policies that were used in the papers, so you can visualize them without training code.

### 2. Testing a policy on a dataset ###
To test a policy on a dataset, go to main/test_policy.m 
The script is self explanatory. The dataset already has pretrained policies that were used in the papers, so you can test them and reproduce numbers in the paper

### 3. Training a policy on a dataset ###
To train a policy, go to main/test_online_policy.m
The script is self explanatory. The script terminates by saving the interim models at all iterations and the results in the local folder. To test the policy you just trained, you have to copy the models in the appropriate folder with the appropriate name. Refer to the pre-trained policies for these naming conventions. Note: Training a policy to convergence will take several hours on a Macbook Pro with 4 parallel pool. 

## Creating a Dataset from Scratch##
If you want to create your own dataset, train a model and test it against baselines, follow these steps. 

### Creating the raw images ###
1. Create a folder in matlab_learning_info_gain_dataset (we call it dataset_5 for example)
2. cd dataset5
3. mkdir data images learnt_predictors precomputed_expert
4. cd images 
5. create train images and call them map_1.png map_2.png ...
6. create test images and call it map_test.png

### Doing data processing ###
1. Go to the source code matlab_learning_info_gain
2. cd dataset_processing
3. Run the script create_data_from_images.m This creates a train and test dataset in dataset_5/data
4. Run the script precompute_expert_on_data.m This creates a roll out on the test dataset using the expert that will be used in aggrevate

### Training the policy ###
Follow the same instructions to train a policy, save it in learnt_predictors and then test it

## Who do I talk to? ##

* Sanjiban Choudhury (sanjiban@cmu.edu)