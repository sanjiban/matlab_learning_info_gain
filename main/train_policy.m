%% TRAIN_POLICY
% This script trains a policy to imitate a clairvoyant oracle.    
clc
clear
close all

%% Load the dataset, grid type and sensor model
dataset = strcat(getenv('dataset_folder'), '/dataset1/');
grid_name = 'exploration_grid';
laser_name = 1;
rng(1);

%% Select the type of policy to evaluate
% This is the most important parameter to select. We have organized
% parameters into sets. Select a set number for global_option_set. This
% determines what problem statement, the feature extractor, the clairvoyant
% oracle, the learner, the precomputed expert and the mixing parameters
% Each set has the following format:
%   max_iters_aggrevate: (Iterations of aggrevate)
%   max_rollout_per_trial: (How many roll outs on one environment)
%   option_problem_setup: 'coverage' / 'motion_constrained_coverage'
%   option_feature: 'ig_pose' / 'ig_pose_motion' (different IG features
%   with motion features)
%   option_learner: 'regression' / 'cs_class' (classifier)
%   option_expert: 'precomputed_expert_greedy' /
%   'precomputed_expert_lazy_gcb' (for roll out in first iter)
%   decision_budget: (number of timesteps)
%   cost_budget: (cost of travel)
%   beta_func: (mixing parameters)
% Sets can have other optional arguments which is documented in the set
% itself.

global_option_set = 3; %Select from the options below

switch global_option_set
    case 1
        % coverage problem with regression
        max_iters_aggrevate = 10;
        max_rollout_per_trial = 1;
        
        option_problem_setup = 'coverage';
        option_feature = 'ig_pose';
        option_qval = 'greedy';
        option_learner = 'regression';
        option_expert = 'precomputed_expert_greedy';
        decision_budget = 30;
        beta_func = @(i) (0.3)^(i-1);
    case 2
        % Coverage problem with cs class (slow)
        max_iters_aggrevate = 10;
        max_rollout_per_trial = 1;
        
        option_problem_setup = 'coverage';
        option_feature = 'ig_pose';
        option_qval = 'greedy';
        option_learner = 'cs_class';
        option_expert = 'precomputed_expert_greedy';
        decision_budget = 30;
        beta_func = @(i) (0.3)^(i-1);
    case 3
        % Constrained coverage problem with regression (EXPLORE presented
        % in ICRA 17)
        max_iters_aggrevate = 1000;
        max_rollout_per_trial = 1;
        
        option_problem_setup = 'motion_constrained_coverage';
        option_feature = 'ig_pose_motion';
        option_qval = 'gcb';
        option_learner = 'regression';
        option_expert = 'precomputed_expert_lazy_gcb';
        decision_budget = 30;
        beta_func = @(i) (0.3)^(i-1);
        cost_budget = 2500;
    case 4
        % Constrained coverage problem with cs class
        max_iters_aggrevate = 100;
        max_rollout_per_trial = 1;
        
        option_problem_setup = 'motion_constrained_coverage';
        option_feature = 'ig_pose_motion';
        option_qval = 'greedy_with_motion';
        option_learner = 'cs_class';
        option_expert = 'precomputed_expert_lazy_gcb';
        decision_budget = 30;
        cost_budget = 2500;
        beta_func = @(i) (0.3)^(i-1);
    otherwise
        error('Bad Option');
end

%% Load all invariant parameters
load(strcat(dataset, 'data/train_data.mat'));
grid_params = get_grid_params(grid_name);
laser_params = get_laser_params(laser_name);
ig_params = get_laser_params(laser_name);
ig_params.laser_resolution = 10*laser_params.laser_resolution;

%% Create problem setup
switch option_problem_setup
    case 'coverage'
        state_transition = @(action, state, vertices, world_map) update_robot_belief( action, state, vertices, world_map, laser_params, grid_params );
        objective_fn = @(state, world_map) occupied_cell_count(state, world_map);
        cost_fn = @(state, vertices, world_map) 1;
        cost_budget = Inf;
    case 'motion_constrained_coverage'
        state_transition = @(action, state, vertices, world_map) update_robot_belief( action, state, vertices, world_map, laser_params, grid_params );
        objective_fn = @(state, world_map) occupied_cell_count(state, world_map);
        cost_fn = @(state, vertices, world_map) motion_cost( state, vertices );
    otherwise
        error('Bad Option');
end

%% Create feature fn
% f (state, action_ids, vertices)
switch option_feature
    case 'ig_pose'
        feature_fn = @(state, action_ids, vertices) [get_ig_features( ig_params.laser_fov,ig_params.laser_resolution,ig_params.max_range,ig_params.range_resolution, vertices(action_ids, 1:2) , vertices(action_ids, 3), state.robot_belief, grid_params) ...
            get_vertex_pose_features( state, action_ids, vertices)];
    case 'ig_pose_motion'
        feature_fn = @(state, action_ids, vertices) [get_ig_features( ig_params.laser_fov,ig_params.laser_resolution,ig_params.max_range,ig_params.range_resolution, vertices(action_ids, 1:2) , vertices(action_ids, 3), state.robot_belief, grid_params) ...
                                                     get_vertex_pose_features( state, action_ids, vertices) ...
                                                     repmat(motion_cost(state, vertices), length(action_ids), 1)];
    otherwise
        error('Bad Option');
end

%% Create Qval fn
% q (state, action_ids, world_map, vertices)
switch option_qval
    case 'greedy'
        qval_fn = @(state, action_ids, world_map, vertices, tau) qval_greedy_objectivefn( state, action_ids, world_map, vertices, objective_fn, state_transition );
    case 'gcb'
        qval_fn = @(state, action_ids, world_map, vertices, tau) qval_gcb( state, action_ids, world_map, vertices, objective_fn, state_transition, cost_budget, tau );
    case 'greedy_with_motion'
        qval_fn = @(state, action_ids, world_map, vertices, tau) qval_greedy_objectivefn_motion( state, action_ids, world_map, vertices, objective_fn, state_transition );
    otherwise
        error('Bad Option');
end

%% Create learner
switch option_learner
    case 'regression'
        data_empty.feature_set = [];
        data_empty.qval = [];
        data_collector = @(data, state, action_ids, vertices, world_map, tau) collect_data_regression_only( data, state, action_ids, world_map, vertices, feature_fn, qval_fn, tau);
        update_learner = @(model, data, old_data) train_rf_for_regress_only(data, 30);
        policy_learner = @(state0, action_ids, vertices, model) predict_rf_qval( state0, action_ids, vertices, feature_fn, model );
        model = [];
    case 'cs_class'
        data_empty.feature_table = [];
        data_empty.cost_table = [];
        data_collector = @(data, state, action_ids, vertices, world_map, tau) collect_data_cs_classification_only( data, state, action_ids, world_map, vertices, feature_fn, qval_fn, tau);
        update_learner = @(model, data, old_data) train_rf_for_csclass_only( data, 30 );
        policy_learner = @(state0, action_ids, vertices, model) predict_rf_cost( state0, action_ids, vertices, feature_fn, model );
        model = [];
    otherwise
        error('Bad Option');
end

%% Create expert
switch option_expert
    case 'precomputed_expert_greedy'
        load(strcat(dataset, 'precomputed_expert/greedy_train.mat'));
        policy_expert = @(state0, action_ids, vertices, world_map, trial) precomputed_action_trajectory{trial};
    case 'precomputed_expert_lazy_gcb'
        load(strcat(dataset, 'precomputed_expert/lazy_gcb_train.mat'));
        policy_expert = @(state0, action_ids, vertices, world_map, trial) precomputed_action_trajectory{trial};
    otherwise
        error('Bad Option');
end

%% Compute stats
compute_train_stats = 1;
eval_frac = round(0.03*length(environment_set));

%% Train a policy using Aggrevate

data = data_empty;
data_collection = [];
old_data = data_empty;
model_collection = [];
obj_train_set = [];
for iters_aggrevate = 1:max_iters_aggrevate
    beta = beta_func(iters_aggrevate); % TODO
    policy_learner_current = @(state, action_ids, vertices)  policy_learner(state, action_ids, vertices, model);
    
    data_new = cell(1, length(environment_set));
    parfor trial = 1:length(environment_set)
        policy_expert_current =  @(state0, action_ids, vertices, world_map) policy_expert(state0, action_ids, vertices, world_map, trial);
        rollout_with_expert_fn = @(state0, action_ids, vertices, world_map, tau) rollout_with_expert( state0, action_ids, vertices, world_map, tau, policy_expert_current, state_transition );
        rollout_with_learner_fn = @(state0, action_ids, vertices, world_map, tau) rollout_with_learner( state0, action_ids, vertices, world_map, tau, policy_learner_current, state_transition, cost_fn, cost_budget );
        rollout_mixture_fn = @(state0, action_ids, vertices, world_map, tau) rollout_mixture( state0, action_ids, vertices, world_map, tau, rollout_with_expert_fn, rollout_with_learner_fn, beta );
        
        for rollout = 1:max_rollout_per_trial
            fprintf('AggrevIter: %d  Trial: %d Rollout: %d\n', iters_aggrevate, trial, rollout);
            
            world_map = environment_set(trial).world_map;
            vertices = environment_set(trial).vertices;
            state0 = environment_set(trial).state;
            original_action_id = 1:length(vertices);
            
            t = randi(decision_budget);
            action_id = setdiff(original_action_id, state0.action_set);
            state = rollout_mixture_fn(state0, action_id, vertices, world_map, t-1);
            %figure(1); plot_robot_belief( state, vertices ); pause;
            action_id = setdiff(original_action_id, state.action_set);
            data_new{trial} = data_collector(data_empty, state, action_id, vertices, world_map, decision_budget-t);
        end
    end
    
    data = data_aggregator( data, data_new, option_learner );
    
    model =  update_learner(model, data, old_data);
    model.feature_fn = feature_fn;
    model_collection = [model_collection model];
    old_data = data;
    data_collection = [data_collection data];
    
    % optionally test the model out and report progress
    if (compute_train_stats)
        policy_learner_current = @(state, action_ids, vertices)  policy_learner(state, action_ids, vertices, model);
        rollout_with_learner_fn = @(state0, action_ids, vertices, world_map, tau) rollout_with_learner( state0, action_ids, vertices, world_map, tau, policy_learner_current, state_transition, cost_fn, cost_budget );
        obj_train = [];
        fprintf('Eval: ');
        for trial = 1:eval_frac
            world_map = environment_set(trial).world_map;
            vertices = environment_set(trial).vertices;
            state0 = environment_set(trial).state;
            original_action_id = 1:length(vertices);
            
            action_id = setdiff(original_action_id, state0.action_set);
            state = rollout_with_learner_fn(state0, action_id, vertices, world_map, decision_budget);
            obj_train = [obj_train objective_fn(state, world_map)];
            fprintf('%d ', trial);
        end
        fprintf('\n');
        obj_train_set = [obj_train_set; obj_train];
        fprintf('AggrevIter: %d  Train Objective Average: %f\n', iters_aggrevate, mean(obj_train));
    end
end

% variables to save : data, data_collection, model(compact),
% model_collection(compact),obj_train_set