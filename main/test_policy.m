%% TEST_POLICY
% This script evaluates learnt policy / clairvoyant oracle on the test
% dataset. It records the performance of the policy on each datapoint in
% objective_trajectory_set which is a Number of timesteps x Num data
clc
clear
close all

%% Load the dataset, grid type and sensor model
dataset = strcat(getenv('dataset_folder'), '/dataset1/');

grid_name = 'exploration_grid'; % Type of grid
laser_name = 1; % Type of laser sensor
rng(1); % fix seed

%% Select the type of policy to evaluate
% This is the most important parameter to select. We have organized
% parameters into sets. Select a set number for global_option_set. This
% determines what problem statement and what policy to evaluate.
% Each set has the following format:
%   option_problem_setup: 'coverage' / 'motion_constrained_coverage' (This
%   denotes the type of problem we are solving)
%   decision_budget: (How many time steps are we evaluating policy to?)
%   cost_budget: (How much travel cost budget does the robot have?)
%   load(...): (Load the learner)
% Sets can have other optional arguments which is documented in the set
% itself.

global_option_set = 5; %Select from the options below
switch global_option_set
    case 1
        % Coverage problem using forward training and cs classification
        option_problem_setup = 'coverage';
        decision_budget = 30;
        load(strcat(dataset, 'learnt_predictors/coverage/forward_training_cs_class.mat'));
        option_policy = 'forward_train_cs_class';
    case 2
        % Coverage problem using forward training and cs classification
        option_problem_setup = 'coverage';
        decision_budget = 30;
        load(strcat(dataset, 'learnt_predictors/coverage/forward_training_reg.mat'));
        option_policy = 'forward_train_reg';
    case 3
        % Coverage problem using IG heuristic
        option_problem_setup = 'coverage';
        decision_budget = 30;
        option_policy = 'heuristic';
        greedy_idx = 4; %Option 1 is average entropy, Option 4 is rear side voxel (the two heuristic used in the papers)
    case 4
        % Coverage problem using aggrevate and regression
        option_problem_setup = 'coverage';
        decision_budget = 30;
        load(strcat(dataset, 'learnt_predictors/coverage/aggrevate_reg.mat'));
        option_policy = 'aggrevate_reg';
    case 5
        % Coverage problem using aggrevate and cs classification
        option_problem_setup = 'coverage';
        decision_budget = 30;
        load(strcat(dataset, 'learnt_predictors/coverage/aggrevate_cs_class.mat'));
        option_policy = 'aggrevate_cs_class';
    case 6
        % Constrained coverage problem using heuristic with motion penalization
        option_problem_setup = 'motion_constrained_coverage';
        decision_budget = 30;
        cost_budget = 2500;
        option_policy = 'heuristic_with_motion';
        greedy_idx = 4; %Option 1 is average entropy, Option 4 is rear side voxel (the two heuristic used in the papers)
    case 7
        % Constrained coverage problem using clairvoyant oracle
        option_problem_setup = 'motion_constrained_coverage';
        decision_budget = 30;
        cost_budget = 2500;
        option_policy = 'lazy_gcb'; %Oracle policy
    case 8
        % Constrained coverage problem using aggrevate and regression
        % (EXPLORE algorithm in ICRA 17)
        option_problem_setup = 'motion_constrained_coverage';
        decision_budget = 30;
        cost_budget = 2500;
        load(strcat(dataset, 'learnt_predictors/motion_constrained_coverage/aggrevate_reg.mat'));
        option_policy = 'aggrevate_reg';
    case 9
        % Constrained coverage problem using aggrevate and cs classification
        option_problem_setup = 'motion_constrained_coverage';
        decision_budget = 30;
        cost_budget = 2500;
        load(strcat(dataset, 'learnt_predictors/motion_constrained_coverage/aggrevate_cs_class.mat'));
        option_policy = 'aggrevate_cs_class';
    otherwise
        error('Bad Option');
end

%% Load all invariant parameters (DO NOT CHANGE)
load(strcat(dataset, 'data/test_data.mat'));
grid_params = get_grid_params(grid_name);
laser_params = get_laser_params(laser_name);
ig_params = get_laser_params(laser_name);
ig_params.laser_resolution = 10*laser_params.laser_resolution;

%% Create problem setup
switch option_problem_setup
    case 'coverage'
        state_transition = @(action, state, vertices, world_map) update_robot_belief( action, state, vertices, world_map, laser_params, grid_params );
        objective_fn = @(state, world_map) occupied_cell_count(state, world_map);
        cost_fn = @(state, vertices, world_map) 1;
        cost_budget = Inf;
    case 'motion_constrained_coverage'
        state_transition = @(action, state, vertices, world_map) update_robot_belief( action, state, vertices, world_map, laser_params, grid_params );
        objective_fn = @(state, world_map) occupied_cell_count(state, world_map);
        cost_fn = @(state, vertices, world_map) motion_cost( state, vertices );
    otherwise
        error('Bad Option');
end

%% Create learner
is_policy_offline = 0;
switch option_policy
    case 'heuristic'
        criteria_fn =  @(state, action_ids, vertices) get_ig_features( ig_params.laser_fov,ig_params.laser_resolution,ig_params.max_range,ig_params.range_resolution, vertices(action_ids, 1:2) , vertices(action_ids, 3), state.robot_belief, grid_params);
        policy = @(iteration, state, action_ids, vertices) greedy_policy( state, action_ids, vertices, criteria_fn, greedy_idx );
    case 'heuristic_with_motion'
        criteria_fn =  @(state, action_ids, vertices) get_ig_features( ig_params.laser_fov,ig_params.laser_resolution,ig_params.max_range,ig_params.range_resolution, vertices(action_ids, 1:2) , vertices(action_ids, 3), state.robot_belief, grid_params);
        policy = @(iteration, state, action_ids, vertices) greedy_policy_with_motion( state, action_ids, vertices, criteria_fn, greedy_idx );
    case 'forward_train_cs_class'
        policy = @(iteration, state, action_ids, vertices) predict_rf_cost( state, action_ids, vertices, model_set{iteration}.feature_fn, model_set{iteration} );
    case 'forward_train_reg'
        policy = @(iteration, state, action_ids, vertices) predict_rf_qval( state, action_ids, vertices, model_set{iteration}.feature_fn, model_set{iteration} );
    case 'aggrevate_reg'
        policy = @(iteration, state, action_ids, vertices) predict_rf_qval( state, action_ids, vertices, model.feature_fn, model );
    case 'aggrevate_cs_class'
        policy = @(iteration, state, action_ids, vertices) predict_rf_cost( state, action_ids, vertices, model.feature_fn, model );
    case 'lazy_gcb'
        policy_expert = @(state0, action_ids, vertices, world_map) lazy_generalized_cost_benefit( action_ids, @(state) objective_fn(state, world_map), @(state) tour_cost( state, vertices ), ...
    state0, @(action, state) state_transition(action, state, vertices, world_map), cost_budget, decision_budget, ...
    @(state) tour_cost( state, vertices ) );
        is_policy_offline = 1;
    otherwise
        error('Bad Option');
end


%% Evaluate policy
objective_trajectory_set = [];
cost_trajectory_set = [];
parfor env_idx = 1:length(environment_set)
    objective_trajectory = [];
    cost_trajectory = [];
    state_trajectory = [];
    
    world_map = environment_set(env_idx).world_map;
    state = environment_set(env_idx).state;
    vertices = environment_set(env_idx).vertices;
    original_action_id = 1:length(vertices);
    
    if (is_policy_offline)
         [~, ~, objective_trajectory, cost_trajectory ] = policy_expert(state, original_action_id, vertices, world_map);
         for i = 1:decision_budget
             fprintf('Evaluation: %d Timestep: %d Obj: %f Cost: %f\n', env_idx, i, objective_trajectory(i), cost_trajectory(i));
         end
    else
        for i = 1:decision_budget
            action_ids = setdiff(original_action_id, state.action_set);
            action_selected = policy(i, state, action_ids, vertices);

            %% Update state and objectives
            state_new = state_transition(action_selected, state, vertices, world_map);
            m_cost = cost_fn(state_new, vertices, world_map);
            if (m_cost <= cost_budget)
                state = state_new;
            end

            val = objective_fn(state, world_map);
            objective_trajectory = [objective_trajectory; val];
            cost_trajectory = [cost_trajectory; m_cost];
            state_trajectory = [state_trajectory; state];

            fprintf('Evaluation: %d Timestep: %d Obj: %f Cost: %f\n', env_idx, i, val, m_cost);
        end
    end
    objective_trajectory_set = [objective_trajectory_set objective_trajectory];
    cost_trajectory_set = [cost_trajectory_set cost_trajectory];
end

%% Optional: Print the test results
if (0)
    load(strcat(dataset, 'data/total_cell.mat')); % Normalization of reward by max cumulative reward
    if (size(objective_trajectory_set,2)==10) %95 CI
        [prctile(objective_trajectory_set(30,:)',20) prctile(objective_trajectory_set(30,:)',50) prctile(objective_trajectory_set(30,:)',80)]/total_cell
    elseif (size(objective_trajectory_set,2)==10) %95 CI
        [prctile(objective_trajectory_set(30,:)',20) prctile(objective_trajectory_set(30,:)',50) prctile(objective_trajectory_set(30,:)',80)]/total_cell
    else
        error('Set 95 perc CI bounds');
    end
end